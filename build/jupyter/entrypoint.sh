#!/bin/env sh

pip install --no-deps -e /src/homography

jupyter-lab --ip=0.0.0.0 --allow-root --no-browser
