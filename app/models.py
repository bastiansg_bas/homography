from typing import Optional
from .validators import url_validator
from pydantic import BaseModel, Field, root_validator


class URLData(BaseModel):
    b_query: str = Field(
        description='black image url',
        example='https://res.cloudinary.com/repaper/image/upload/v1567812585/SALAS/SALAS_73/PNG/COLOR1/salas_73_000000.png'
    )

    w_query: str = Field(
        description='white image url',
        example='https://res.cloudinary.com/repaper/image/upload/v1567812585/SALAS/SALAS_73/PNG/COLOR1/salas_73_FFFFFF.png'
    )

    train: str = Field(
        description='color image url',
        example='https://res.cloudinary.com/repaper/image/upload/v1569879107/SALAS/SALAS_73/JPG/salas_73.jpg'
    )

    @root_validator
    def valid_url(cls, values):
        b_query_is_valid = url_validator(values.get('b_query'))
        w_query_is_valid = url_validator(values.get('w_query'))
        train_is_valid = url_validator(values.get('train'))

        if not b_query_is_valid:
            raise ValueError('b_query must by a valid url')

        if not w_query_is_valid:
            raise ValueError('w_query must by a valid url')

        if not train_is_valid:
            raise ValueError('train must by a valid url')

        return values


class Centroid(BaseModel):
    centroid: Optional[list] = Field(
        description='x and y coordinates of the centroid',
        example=[402, 60]
    )
