import sys
import time
import logging

from fastapi import FastAPI
from .models import URLData, Centroid

from homography.homography import Homography
from homography.utils.image import url_data_preprocess


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


homography = Homography()
app = FastAPI()


@app.post('/image_centroid/', response_model=Centroid)
def get_image_centroid(url_data: URLData) -> Centroid:
    start = time.time()
    image_data = url_data_preprocess(url_data.dict())
    centroid = {
        'centroid': homography.get_centroid(image_data)
    }

    time_request = round(time.time() - start, 2)
    logger.info(f'time_request => {time_request}s')

    return centroid
