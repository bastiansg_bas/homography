import os

from glob import glob
from subprocess import check_call
from distutils.core import run_setup


SRC_PATH = os.getenv('SRC_PATH', '/tmp/src')


def install_requirements(setup_path: str):
    result = run_setup(setup_path, stop_after='init')
    requirements = result.install_requires
    check_call(['pip', 'install', *requirements])


def main():
    setup_files = glob(f'{SRC_PATH}/**/setup.py')
    for setup_file in setup_files:
        install_requirements(setup_file)


if __name__ == '__main__':
    main()
