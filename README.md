# Homography

Setup
-----

You need to install git-lfs before cloning the repo

```bash
$ git clone https://gitlab.com/bastiansg_bas/homography
```

This repo requires Docker 20.10 or above

If you want to run the inferences on a GPU you also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit on linux as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

API
---

#### Build app
```bash
$ make app-build
```
#### Run app
```bash
$ make app-run
```
#### Run app on GPU (you need to install nvidia-container-toolkit)
```bash
$ make app-run-gpu
```
#### API documentation
    http://localhost:8000/docs
<br/>

Jupyter
-------

#### Build jupyter
```bash
$ make jupyter-build
```
#### Run jupyter
```bash
$ make jupyter-run
```
#### Run jupyter on GPU (you need to install nvidia-container-toolkit)
```bash
$ make jupyter-run-gpu
```
<br/>

Optional
----------------
#### Enviroment setup (you need to install pipenv)

```bash
$ make env-setup
```
