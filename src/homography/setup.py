from setuptools import find_packages, setup

setup(
    name='homography',
    packages=find_packages(),
    version='1.0.0',
    description='',
    author='Bas',
    author_email='bastiansg.bas@gmail.com',
    url='https://gitlab.com/bastiansg_bas/homography',
    install_requires=[
        'Pillow==8.1.2',
        'shapely==1.7.1',
        'aiohttp==3.7.4'
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3'
    ]
)
