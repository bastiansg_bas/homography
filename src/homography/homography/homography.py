import cv2
import logging

import numpy as np

from typing import Optional
from shapely.geometry import MultiPoint


# ref: https://docs.opencv.org/master/d1/de0/tutorial_py_feature_homography.html
# FLANN - Fast Library for Approximate Nearest Neighbors
FLANN_INDEX_KDTREE = 1
SCH_PARAM_CHECKS = 50
INDEX_PARAM_TREES = 5
GOOD_MATCH_THRESHOLD = 0.7
MIN_MATCH_COUNT = 10


logger = logging.getLogger(__name__)


class Homography():
    def __init__(self):
        self.sift = cv2.SIFT_create()

    def get_match_point(self, good_match: cv2.DMatch) -> tuple:
        point = self.kp2[good_match.trainIdx].pt
        return point

    def get_centroid_data(
            self,
            query_image: np.ndarray,
            train_image: np.ndarray) -> dict:

        kp1, desc1 = self.sift.detectAndCompute(query_image, None)
        self.kp2, desc2 = self.sift.detectAndCompute(train_image, None)

        index_params = dict(
            algorithm=FLANN_INDEX_KDTREE,
            trees=INDEX_PARAM_TREES
        )

        # number of times the trees in the index should be recursively traversed
        # Higher values gives better precision, but also takes more time
        sch_params = dict(checks=SCH_PARAM_CHECKS)
        flann = cv2.FlannBasedMatcher(index_params, sch_params)

        matches = flann.knnMatch(desc1, desc2, k=2)

        # select good matches
        good_matches = [
            m for m, n in matches
            if m.distance < GOOD_MATCH_THRESHOLD * n.distance
        ]

        matches_found = len(good_matches)
        logger.info(f'{matches_found} matches found')

        if matches_found < MIN_MATCH_COUNT:
            logger.warn('not enough matches found')

        centroid = None
        if matches_found:
            train_pts = [self.get_match_point(x) for x in good_matches]
            points = MultiPoint(train_pts)
            centroid = points.centroid.coords[0]
            centroid = [int(x) for x in centroid]

        centroid_data = {
            'centroid': centroid,
            'matches_found': matches_found
        }

        return centroid_data

    def get_centroid(self, image_data: dict) -> Optional[tuple]:

        b_query_image = image_data['b_query']
        w_query_image = image_data['w_query']
        train_image = image_data['train']

        logger.info('computing homography with b_query_image')
        b_centroid_data = self.get_centroid_data(b_query_image, train_image)

        logger.info('computing homography with w_query_image')
        w_centroid_data = self.get_centroid_data(w_query_image, train_image)

        b_matches_found = b_centroid_data['matches_found']
        w_matches_found = w_centroid_data['matches_found']

        if not b_matches_found and not w_matches_found:
            return

        if b_centroid_data['matches_found'] > w_centroid_data['matches_found']:
            logger.info('bests result with b_query_image')
            return b_centroid_data['centroid']

        logger.info('best result with w_query_image')
        return w_centroid_data['centroid']
