import aiohttp
import asyncio
import logging

from io import BytesIO


logger = logging.getLogger(__name__)


async def get_image_response(url_item: dict) -> dict:
    async with aiohttp.ClientSession() as session:
        url = url_item['image_url']

        async with session.get(url) as r:
            status = r.status
            if status != 200:
                logger.error(f'{url} estatus code {status}')
                return

            response = await r.read()

    image_response = {
        'image_stream': BytesIO(response),
        'image_name': url_item['image_name']
    }

    return image_response


async def get_image_responses(url_data: dict) -> dict:
    urls = [{'image_name': k, 'image_url': v} for k, v in url_data.items()]
    tasks = map(
        lambda url: asyncio.ensure_future(get_image_response(url)),
        urls
    )

    image_responses = await asyncio.gather(*tasks)
    image_responses = filter(bool, image_responses)
    image_responses = {
        x['image_name']: x['image_stream']
        for x in image_responses
    }

    return image_responses
