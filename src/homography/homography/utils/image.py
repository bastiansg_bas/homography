import cv2
import asyncio

import numpy as np

from PIL import Image, ImageDraw
from typing import Union, BinaryIO
from homography.utils.req import get_image_responses


CENTROID_RADIUS = 10


def cv2pil(cv_image: np.ndarray) -> Image.Image:
    image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(image)
    return pil_image


def pil2cv(pil_image: Image.Image) -> np.ndarray:
    image = np.array(pil_image)
    cv_image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return cv_image


def io2cv(image_stream: BinaryIO) -> np.ndarray:
    image = np.fromstring(image_stream.read(), np.uint8)
    image = cv2.imdecode(image, 1)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image


def draw_centroid(image: np.ndarray, centroid: tuple) -> Image.Image:

    ellipse = (
        centroid[0] - CENTROID_RADIUS,
        centroid[1] - CENTROID_RADIUS,
        centroid[0] + CENTROID_RADIUS,
        centroid[1] + CENTROID_RADIUS
    )

    image = cv2pil(image)
    draw = ImageDraw.Draw(image)
    draw.ellipse(ellipse, outline='red', width=3)
    return image


def transform_w_query_image(
        image_path_or_stream: Union[str, BinaryIO]) -> np.ndarray:

    image = Image.open(image_path_or_stream)
    new_image = Image.new('RGBA', image.size, 'BLACK')
    new_image.paste(image, (0, 0), image)
    new_image = new_image.convert('RGB')
    cv_image = pil2cv(new_image)
    cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
    return cv_image


def image_data_preprocess(image_data: dict) -> dict:
    image_data = {
        'train': cv2.imread(image_data['train'], 0),
        'b_query': cv2.imread(image_data['b_query'], 0),
        'w_query': transform_w_query_image(image_data['w_query'])
    }

    return image_data


def url_data_preprocess(url_data: dict) -> dict:
    image_responses = asyncio.run(get_image_responses(url_data))
    image_data = {
        'train': io2cv(image_responses['train']),
        'b_query': io2cv(image_responses['b_query']),
        'w_query': transform_w_query_image(image_responses['w_query'])
    }

    return image_data
