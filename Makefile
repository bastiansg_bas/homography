PROYECT = homography
JUPYTER_IMAGE = $(PROYECT)-jupyter
APP_IMAGE = $(PROYECT)-app

env-setup:
	pipenv install --skip-lock

jupyter-build:
	docker build -t $(JUPYTER_IMAGE) -f ./build/jupyter/Dockerfile .

jupyter-run:
	docker run -it --rm -p 8888:8888 \
	-v $(PWD)/notebooks:/jupyter \
	-v $(PWD)/src:/src \
	-v $(PWD)/resources:/resources \
	$(JUPYTER_IMAGE)

jupyter-run-gpu:
	docker run -it --rm -p 8888:8888 \
	--gpus all \
	--shm-size 8G \
	-v $(PWD)/notebooks:/jupyter \
	-v $(PWD)/src:/src \
	-v $(PWD)/resources:/resources \
	$(JUPYTER_IMAGE)

app-build:
	docker build -t $(APP_IMAGE) -f ./build/app/Dockerfile .

app-run:
	docker run -it --rm -p 8000:8000 \
	-v $(PWD)/resources:/resources $(APP_IMAGE)

app-run-gpu:
	docker run -it --rm -p 8000:8000 \
	--gpus all \
	--shm-size 8G \
	-v $(PWD)/resources:/resources $(APP_IMAGE)
